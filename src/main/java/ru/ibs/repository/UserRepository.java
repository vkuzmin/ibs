package ru.ibs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ibs.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
