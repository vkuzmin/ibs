package ru.ibs.service;

import ru.ibs.domain.User;

public interface UserService {

    User findByUsername(String username);
}
